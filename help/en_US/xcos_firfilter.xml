<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="xcos_firfilter">
  <refnamediv>
    <refname>xcos_firfilter</refname>
    <refpurpose>Design an FIR filter and filter the input signal</refpurpose>
  </refnamediv>

  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../images/gif/xcos_firfilter.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>

  <refsection id="Contents_xcos_firfilter">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <xref linkend="Description_xcos_firfilter">Description</xref>
        </para>
      </listitem>
      <listitem>
        <para>
          <xref linkend="Data_types_xcos_firfilter">Data types</xref>
        </para>
      </listitem>
      <!--
      <listitem>
        <para>
          <xref linkend="Properties_box_xcos_firfilter">Default Properties</xref>
        </para>
      </listitem>
      -->
      <listitem>
        <para>
          <xref linkend="Example_xcos_firfilter">Example</xref>
        </para>
      </listitem>
      <listitem>
        <para>
          <xref linkend="Interfacingfunction_xcos_firfilter">Interfacing function</xref>
        </para>
      </listitem>
      <listitem>
        <para>
          <xref linkend="Computationalfunction_xcos_firfilter">Computational function</xref>
        </para>
      </listitem>
      <listitem>
        <para>
          <xref linkend="SeeAlso_xcos_firfilter">See also</xref>
        </para>
      </listitem>
      <listitem>
        <para>
          <xref linkend="Author_xcos_firfilter">Author</xref>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_xcos_firfilter">
    <title>Description</title>
    <para>
        This block enables the user to design a digital FIR (infinite impulse
        response) filter and process the input signal using the filter. The user
        can design the filter using 5 methods:
    </para>

    <para>
        <inlinemediaobject>
            <imageobject>
                <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog.gif" valign="middle"/>
            </imageobject>
        </inlinemediaobject>
    </para>
    <itemizedlist>
        <para>
            <listitem>Enter filter coefficients manually</listitem>
        </para>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog1.gif" valign="middle"/>
                </imageobject>
            </inlinemediaobject>
        </para>
        <para>
            <listitem>Use Scilab wfir function</listitem>
        </para>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog2.gif" valign="middle"/>
                </imageobject>
            </inlinemediaobject>
        </para>
        <para>
            <listitem>Enter magnitude of sampled frequency response (frequency
                sampling method)</listitem>
        </para>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog3.gif" valign="middle"/>
                </imageobject>
            </inlinemediaobject>
        </para>
        <para>
            <listitem>Use Remez algorithm (Scilab remezb function)</listitem>
        </para>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog4.gif" valign="middle"/>
                </imageobject>
            </inlinemediaobject>
        </para>
        <para>
            <listitem>Use equiripple method (Scilab eqfir function)</listitem>
        </para>
        <para>
            <inlinemediaobject>
                <imageobject>
                    <imagedata align="center" fileref="../../images/gif/xcos_firfilter_dialog5.gif" valign="middle"/>
                </imageobject>
            </inlinemediaobject>
        </para>
    </itemizedlist>
  </refsection>
  <refsection id="Data_types_xcos_firfilter">
    <title>Data types</title>
    <itemizedlist>
      <listitem>
        <para>Input: A scalar every clock cycle</para>
        <para>Output: A scalar every clock cycle</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <!--
  <refsection id="Properties_box_xcos_firfilter">
    <title>Default properties</title>
    <para>first = 16</para>
    <para>second = 'red'</para>
    <para>third = '[12;23]'</para>
  </refsection>
  -->
  <refsection id="Example_xcos_firfilter">
    <title>Example</title>
    <para>
        <inlinemediaobject>
            <imageobject>
                <imagedata align="center" fileref="../../images/gif/xcos_firfilter_demo.gif" valign="middle"/>
            </imageobject>
        </inlinemediaobject>
    </para>
    <para>This example can be found under: {dspblock Toolbox}/demos/firfilterblock.xcos</para>
  </refsection>
  <refsection id="Interfacingfunction_xcos_firfilter">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/xcos_firfilter.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_xcos_firfilter">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/comp_firfilter.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_xcos_firfilter">
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="xcos_firfilter">xcos_firfilter</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/wfir">Link to the Scilab help page for the wfir function.</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/remezb">Link to an Scilab help page for the remezb function</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/eqfir">Link to an Scilab help page for the eqfir function</link>
      </member>
    </simplelist>
  </refsection>
  <refsection id="Author_xcos_firfilter">
    <title>Author</title>
    <simplelist type="inline">
      <member>Sumeet Katariya</member>
    </simplelist>
  </refsection>
</refentry>
