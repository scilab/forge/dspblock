extern "C"
{
    #include "scicos_block4.h"
}
#include <vector>
#include <iterator>
#include "statistics.h"
#include "linearalgebra.h"
#include <cmath>

extern "C" void rms_c(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, i = 0;

    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    u = GetRealInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    std::vector<double> v(u, u+mu*nu);  //Get matrix elements into a vector
    std::vector<double> w(v.size());
    linearalgebra::transpose(v, mu, nu, w);
    std::vector<double>::iterator iter = w.begin();
    std::vector<double> x(nu);

    for (i=0; i<mu; ++i) {
        x.assign(iter, iter+nu);
        y[i] = std::sqrt((statistics::sumsq(x))/nu);
        iter += nu;
    }
}
