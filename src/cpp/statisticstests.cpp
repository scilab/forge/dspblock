#include "statistics.h"
#include <cmath>
#include <iostream>
#include <assert.h>
#define EPS 1e-3
int main()
{
    double a[] = {1, 2, 3, 4, 5};
    std::vector<double> v(a, a+5);
    assert(statistics::variance(v) == 2.5);
    assert(statistics::sumsq(v) == 55);

    double b[] = {1};
    std::vector<double> w(b, b+1);
    assert(statistics::variance(w) == 0);
    assert(statistics::sumsq(w) == 1);
    return 0;
}
