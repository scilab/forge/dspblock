#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void max_m(scicos_block *block, int flag)
{
    double *dInput = NULL;
    double *dValue = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInput = GetRealInPortPtrs(block, 1);
    dValue = GetRealOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> v(dInput, dInput + nInputRows*nInputCols);  //Get matrix elements into a vector
    std::vector<double>::iterator dPosMax = std::max_element(v.begin(),v.end());
    dValue[0] = *dPosMax;
    nIndex[0] = std::distance(v.begin(),dPosMax);

}
