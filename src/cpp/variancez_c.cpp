extern "C"
{
    #include "scicos_block4.h"
}
#include <vector>
#include <iterator>
#include "statistics.h"
#include "linearalgebra.h"

extern "C" void variancez_c(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, i = 0;

    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    ur = GetRealInPortPtrs(block, 1);
    ui = GetImagInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    std::vector<double> vreal(ur, ur+mu*nu);  //Get real matrix elements into a vector
    std::vector<double> vimag(ui, ui+mu*nu);  //Get imag matrix elements
    std::vector<double> wreal(vreal.size());
    std::vector<double> wimag(vimag.size());
    linearalgebra::transpose(vreal, mu, nu, wreal);
    linearalgebra::transpose(vimag, mu, nu, wimag);

    std::vector<double>::iterator realiter = wreal.begin();
    std::vector<double>::iterator imagiter = wimag.begin();
    std::vector<double> xreal(nu);
    std::vector<double> ximag(nu);

    for (i=0; i<mu; ++i) {
        xreal.assign(realiter, realiter+nu);
        ximag.assign(imagiter, imagiter+nu);
        y[i] = statistics::variance(xreal) + statistics::variance(ximag);
        realiter += nu;
        imagiter += nu;
    }
}


