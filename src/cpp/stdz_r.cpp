extern "C"
{
    #include "scicos_block4.h"
}
#include <vector>
#include <iterator>
#include "statistics.h"
#include "linearalgebra.h"
#include <cmath>

extern "C" void stdz_r(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, j = 0;

    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    ur = GetRealInPortPtrs(block, 1);
    ui = GetImagInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    std::vector<double> vreal(ur, ur+mu*nu);  //Get real matrix elements into a vector
    std::vector<double> vimag(ui, ui+mu*nu);  //Get imag matrix elements

    std::vector<double>::iterator realiter = vreal.begin();
    std::vector<double>::iterator imagiter = vimag.begin();
    std::vector<double> xreal(mu);
    std::vector<double> ximag(mu);

    for (j=0; j<nu; ++j) {
        xreal.assign(realiter, realiter+mu);
        ximag.assign(imagiter, imagiter+mu);
        y[j] = std::sqrt(statistics::variance(xreal) + statistics::variance(ximag));
        realiter += mu;
        imagiter += mu;
    }
}
