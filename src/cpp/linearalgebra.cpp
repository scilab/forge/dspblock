#include <vector>
#include <iostream>
#include <iterator>
#include "linearalgebra.h"

void linearalgebra::transpose(std::vector<double> &v, int nrows, int ncols, std::vector<double> &w) {
    //return error if nrows * ncols != v.size()
    //handle empty array, unit row, unit column, etc
    //make ncols optional
    int i, j;
    std::vector<double>::iterator viter = v.begin();
    std::vector<double>::iterator witer = w.begin();

    for (j=0; j<ncols; ++j) {
        for (i=0; i<nrows; ++i) {
            *(witer+i*ncols+j) = *viter;    //ncols is the nrows for the transpose matrix
            ++viter;
        }
    }
}


void linearalgebra::printmatrix(std::vector<double>& v,int nrows,int ncols) {
    std::vector<double>::iterator viter = v.begin();
    int i,j;
    for (i=0; i<nrows; i++) {
        for (j=0; j<ncols; j++) {
            std::cout << *(viter+j*nrows+i) << " ";
        }
        std::cout << std::endl;
    }
}
