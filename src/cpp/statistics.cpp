#include <iostream>
#include "statistics.h"
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>

using namespace std;
double statistics::variance(const vector<double> &v)
{
    if (v.size() == 1 )
        return 0;
    vector<double> diff(v.size());

    double sum = accumulate(v.begin(), v.end(), 0.0);
    double mean = sum/v.size();

    transform(v.begin(), v.end(), diff.begin(), bind2nd(minus<double>(), mean));
    double sqsum = inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);

    return sqsum/(v.size()-1);
}

double statistics::sumsq(const vector<double> &v)
{
    return inner_product(v.begin(), v.end(), v.begin(), 0.0);
}
