extern "C"
{
    #include "scicos_block4.h"
}
#include <vector>
#include <iterator>
#include "statistics.h"

extern "C" void variance_r(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, j = 0;

    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    u = GetRealInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    std::vector<double> v(u, u+mu*nu);  //Get matrix elements into a vector
    std::vector<double>::iterator iter = v.begin();
    std::vector<double> x(mu);

    for (j=0; j<nu; ++j) {
        x.assign(iter, iter+mu);
        y[j] = statistics::variance(x);
        iter += mu;
    }
}


