#include "scicos_block4.h"
#include "stdlib.h"

void runningmean(scicos_block *block, int flag)
{
    int rows = GetInPortRows(block,1);
    int cols = GetInPortCols(block,1);
    double *u = GetRealInPortPtrs(block,1);
    double *y = GetRealOutPortPtrs(block,1);
    double time;

    int i,j;

    switch(flag)
    {
        case Initialization: //flag=4
            for (j=0; j<cols; ++j)
            {
                for (i=0; i<rows; ++i)
                {
                    y[i+j*rows] = 0;
                }
            }
            block->x[0] = 0;
            break;

        case OutputUpdate://flag = 1
            if (GetNevIn(block) == 2 | GetNevIn(block) == 3)
            {
                block->x[1] = 1;
            }
            if (GetNevIn(block) == 1 | GetNevIn(block) == 3)
            {
                if (block->x[1] == 1)
                {
                    for (j=0; j<cols; ++j)
                    {
                        for (i=0; i<rows; ++i)
                        {
                            if (i==0) {
                                y[i+j*rows] = u[i+j*rows];
                            }
                            else {
                                y[i+j*rows] = (u[i-1 + j*rows]*i + u[i + j*rows])/(i+1);
                            }
                        }
                    }
                    block->x[0] = 1;
                    block->x[1] = 0;
                }
                else
                {
                    time = block->x[0];
                    for (j=0; j<cols; ++j)
                    {
                        for (i=0; i<rows; ++i)
                        {
                            if (i==0) {
                                y[i+j*rows] = (u[i+j*rows] + time*rows*y[j*rows + rows-1])/(time*rows+1);
                            }
                            else {
                                y[i+j*rows] = (y[i-1 + j*rows]*(time*rows + i) + u[i + j*rows])/(time*rows+i+1);
                            }
                        }
                    }
                    block->x[0] += 1; //how many seconds are over
                }
            }
    }
}
