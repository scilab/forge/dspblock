#include "scicos_block4.h"

void meanz_m(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *yr = NULL;
    double *yi = NULL;
    int nu = 0, mu = 0, j = 0;
    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);
    ur = GetRealInPortPtrs(block,1);
    ui = GetImagInPortPtrs(block,1);
    yr = GetRealOutPortPtrs(block,1);
    yi = GetImagOutPortPtrs(block,1);
    
    yr[0] = 0;
    yi[0] = 0;
    for (j=0; j<mu*nu; j++)
    {
        yr[0] += ur[j];
        yi[0] += ui[j];
    }
    yr[0] /= (mu*nu);
    yi[0] /= (mu*nu);
}
