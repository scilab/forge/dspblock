// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_c()
  src_c_path = get_absolute_file_path("builder_c.sce");

  CFLAGS = ilib_include_flag(src_c_path);
  LDFLAGS = "";
  if (getos()<>"Windows") then
    if ~isdir(SCI+"/../../share") then
      // Source version
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes" ;
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes" ;
    else
      // Release version
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos_blocks";
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos";
    end
  else
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes";
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes";

    // Getting symbols
    if findmsvccompiler() <> "unknown" & haveacompiler() then
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos.lib""";
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos_f.lib""";
    end
  end

  tbx_build_src(["mean_m", "mean_r", "mean_c", "meanz_m", "meanz_r", "meanz_c", "buffer_vect_xcos", "runningmean"],        ..
                ["mean_m.c", "mean_r.c", "mean_c.c", "meanz_m.c", "meanz_r.c", "meanz_c.c", "buffer_vect_xcos.c", "runningmean.c"],    ..
                "c",                                  ..
                src_c_path,                           ..
                "",                                   ..
                LDFLAGS,                              ..
                CFLAGS,                               ..
                "",                                   ..
                "",                                   ..
                ""); //not sure what goes here
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
