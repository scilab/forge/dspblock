#include "scicos_block4.h"

void mean_c(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, i = 0, j = 0;
    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);

    u = GetRealInPortPtrs(block,1);
    y = GetRealOutPortPtrs(block,1);

    for (i=0; i<mu; i++)
    {
        y[i] = 0;
        for (j=0; j<nu; j++)
        {
            y[i] += u[mu*j+i];
        }
        y[i] /= nu;
    }
}
