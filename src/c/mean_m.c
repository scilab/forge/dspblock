#include "scicos_block4.h"
void mean_m(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, j = 0;
    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    u = GetRealInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    y[0] = 0;
    for (j=0; j<mu*nu; j++)
    {
        y[0] += u[j];
    }
    y[0] /= (mu*nu);
}

