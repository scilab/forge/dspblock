#include "scicos_block4.h"

void meanz_r(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *yr = NULL;
    double *yi = NULL;
    int mu=0, nu=0, i=0, j=0;

    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);
    ur = GetRealInPortPtrs(block,1);
    ui = GetImagInPortPtrs(block,1);
    yr = GetRealOutPortPtrs(block,1);
    yi = GetImagOutPortPtrs(block,1);

    for (j=0; j<nu; j++)
    {
        yr[j] = 0;
        yi[j] = 0;
        for (i=0; i<mu; i++)
        {
            yr[j] += ur[i+j*mu];
            yi[j] += ui[i+j*mu];
        }
        yr[j] /= mu;
        yi[j] /= mu;
    }
}
