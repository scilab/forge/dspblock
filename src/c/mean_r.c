#include "scicos_block4.h"
#include "MALLOC.h"

void mean_r(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, i = 0, j = 0, ij = 0;

    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);

    u = GetRealInPortPtrs(block,1);
    y = GetRealOutPortPtrs(block,1);

    for (j=0; j<nu; j++)
    {
        y[j] = 0;
        for (i=0; i<mu; i++)
        {
            y[j] += u[mu*j+i];
        }
        y[j] /= mu;
    }
}
