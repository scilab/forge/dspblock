path = get_absolute_file_path('meanblock2.tst');

status = importXcosDiagram(path + '/meanblock1.xcos');
if ~status then pause, end
realmatrix = eval(scs_m.objs(3).graphics.exprs);
scs_m.objs(4).graphics.exprs = ["1";"1"];
xcos_simulate(scs_m, 4);
b = a1('values');
c = mean(realmatrix);
assert_checkequal(b,c);

