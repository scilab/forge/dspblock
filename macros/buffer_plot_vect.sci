function block = buffer_plot_vect(block, flag)
  win = 20000 + curblock();
  // flag = 4: initialization. We display the plot window with a 0 line
  if flag == 4 then
    H = scf(win);
    clf(H);
    t = 1:length(block.inptr(1));
    x = zeros(block.inptr(1));
    plot(t,x);
    xtitle('Plot vect');
  // flag = 1 and block.nevprt>0: xcos asks for the output and an
  //         event appears on the event port, then we display the
  //         curve. We store the points directly into the data
  //         variable in the graphical handle so as to avoid to	redraw
  //         the complete window. This avoid filckering of the graph
  //         window
  elseif (flag == 1) & (block.nevprt>0) then
    H = scf(win);
    Color = block.ipar(1);
    Line_style = block.ipar(2)
    t = 1:length(block.inptr(1));
    x = block.inptr(1);
    H.children(1).children(1).children(1).data = [t' x'];
    H.children(1).children(1).children(1).line_style = Line_style;
    H.children(1).children(1).children(1).foreground = Color;
  end
endfunction
