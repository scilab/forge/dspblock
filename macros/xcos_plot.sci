//  Scicos
//
//  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// See the file ../license.txt
//

function [x,y,typ] = xcos_plot(job,arg1,arg2)
  x = []; y = []; typ = [];
  select job
    case 'plot' then
      standard_draw(arg1);
    case 'getinputs' then
      [x,y,typ]=standard_inputs(arg1);
    case 'getoutputs' then
      x = []; y = []; typ = [];
    case 'getorigin' then
      [x,y] = standard_origin(arg1);
    case 'set' then
      x        = arg1;
      graphics = arg1.graphics;
      exprs    = graphics.exprs;
      model    = arg1.model;
    
      while %t do
        [ok,Color,Line_type,exprs] = scicos_getvalue(..
	    	'Set Plot vect parameters',..
		['Color';'Line type'],..
		list('vec',1,'vec',1),..
		exprs);
        if ~ok then break,end //user cancel modification
        
        if ok then
          if Color==[] then Color = 1; end
          if Line_type==[] then Line_type = 1; end
    
          model.rpar     = [];
          model.ipar     = [Color; Line_type];
          model.label    = 'Plot vect';
      	  model.sim      = list('buffer_plot_vect',5);
          model.in       = 1;
      	  model.in2      = -1;
          model.evtin    = 1;
          graphics.id    = 'Plot vect';
          graphics.exprs = exprs;
          x.graphics     = graphics;
          x.model        = model;
          break;
        end
      end
    case 'define' then
      Color     = 1;
      Line_type = 1;
    
      model = scicos_model();
      model.rpar   = [];
      model.ipar   = [Color; Line_type]
      model.label  = 'Plot vect';
      model.sim    = list('buffer_plot_vect',5);
      model.in     = 1;
      model.in2    = -1;
      model.evtin  = 1;
      model.blocktype = 'd';
      model.dep_ut = [%f %f];
    
      exprs = [string(Color); ..
    	       string(Line_type)];
    
      gr_i = ['thick=xget(''thickness'');xset(''thickness'',2);';
              'xrect(orig(1)+sz(1)/10,orig(2)+(1-1/10)*sz(2),sz(1)*8/10,sz(2)*8/10);';
              'xset(''thickness'',thick)'];
    
      x = standard_define([2 2],model,exprs,gr_i);
  end
endfunction
