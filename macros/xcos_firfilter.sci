function [x,y,typ] = xcos_firfilter(job,arg1,arg2)

    x=[];y=[];typ=[];


    select job

    case 'plot' then
        standard_draw(arg1);

    case 'getinputs' then
        [x,y,typ]=standard_inputs(arg1);

    case 'getoutputs' then
        [x,y,typ]=standard_outputs(arg1);

    case 'getorigin' then
        [x,y]=standard_origin(arg1);

    case 'set' then
        x=arg1;
        model = arg1.model;
        graphics = arg1.graphics;
        exprs = graphics.exprs;

        ok = %t;
        while %t do

            [ok, option, exprs(1)] = getvalue(['How to design filter?'; '1. Enter coefficients manually'; '2. Use Scilab filter design tool'; '3. Use frequency sampling method'; '4. Use Remez (Parks-McClellan) algorithm'; '5. Use minimax equiripple method'],..
            ['Enter option'],..
            list('vec',1), exprs(1));

            if (option<1 | option>5)
                message("Enter option 1 to 5");
                ok = %f;
            end

            if ~ok
                break;
            end

            if (option==1) then
                [ok, coeff, exprs(2)] = getvalue("Set filter coefficients",..
                             ["Filter coefficients"],..
                             list("vec",-1), exprs(2));
            end

            if (option==2) then
                [ok, ftype, forder, cfreq, wtype, fpar, exprs(3:7)] = getvalue('Enter filter parameters (Refer to help page of wfir function)',..
                ['Type of filter (lp:lowpass, hp:highpass, bp:bandpass, sb:bandstop';..
                'Order of filter';..
                'Normalized cutoff freq (between 0 and 0.5)';..
                'Window type (re:Rectangular, tr:Triangular, hm:Hamming, hn:Hanning, kr:Kaiser, ch:Chebyshev)';..
                'Filter parameters (only for Kaiser and Chebyshev)'],..
                list('str',-1,'vec',1,'vec',-1,'str',-1,'vec',-1),..
                exprs(3:7));

                [coeff, wfm, fr] = wfir(ftype, forder, cfreq, wtype, fpar);
            end

            if (option == 3) then
                [ok, samples, exprs(8)] = getvalue('Enter magnitude of sampled frequency response',..
                ['Magnitude of sampled frequency response'],..
                list('vec',-1),..
                exprs(8));

                N = 2*length(samples)-1;
                k1 = [0:(N-1)/2];
                phase = -(N-1)/2 * 2*%pi*k1/N;
                k2 = [(N+1)/2:N-1];
                phase($+1: $+length(k2)) = (N-k2) * 2*%pi/N * (N-1)/2;
                freq = [0 : 2*%pi/N : 2*%pi-%pi/N];
                mag = samples;
                mag($+1:$+length(samples)-1) = samples($:-1:2);
                H = mag.*exp(%i*phase);
                coeff = ifft(H);
                coeff = real(coeff);
            end

            if (option == 4)
                while %t do
                    [ok, nc, fg, ds, exprs(9:11)] = getvalue('Refer to help for the function remezb',..
                    ['Filter length (nc)'; 'Frequency samples(fg)'; 'Magnitude of frequency response at the frequency samples(ds)'],..
                    list('vec',1,'vec',-1,'vec',-1),..
                    exprs(9:11));

                    if (length(fg)~=length(ds)) then
                        message('Length of fg and ds should be equal');
                        ok = %f;
                    else
                        wt = ones(1,length(ds));
                        an = remezb(nc,fg,ds,wt);
                        coeff(1:nc-1) = an(nc:-1:2)/2;
                        coeff(nc) = an(1);
                        coeff(nc+1:2*nc-1) = an(2:nc)/2;
                        break;
                    end
                end
            end

            if (option == 5) then
                while %t do
                    [ok, nf, bedge, des, exprs(12:14)] = getvalue('Refer to help for the function eqfir',..
                        ['Filter length(nf)'; 'Edge frequencies for each band(bedge)'; 'Desired magnitude for each band:  1 or 0(des)'],..
                        list('vec',1,'vec',-1,'vec',-1),..
                        exprs(12:14));

                    if (size(bedge,1)~=length(des)) then
                        message('Length of des should be equal to the number of bands');
                        ok = %f;
                    else
                        wt = ones(1,length(des));
                        coeff  = eqfir(nf, bedge, des, wt);
                        break;
                    end
                end
            end
            
            if ~ok
                break;
            end

            model.rpar = coeff;
            model.state = zeros(1,length(coeff));
            junction_name = "comp_firfilter";
            it = 1;
            ot = 1;
            in = [model.in model.in2];
            out = [model.in model.in2];

            if ok
                [model, graphics, ok] = set_io(model, graphics, list(in,it), list(out,ot), [], []);
            end

            if ok then
                model.sim       = list(junction_name, 5);
                arg1.model      = model;
                graphics.exprs  = exprs;
                arg1.graphics   = graphics;
                x               = arg1;
                break;
            end
        end

    case 'define' then
        model=scicos_model();
        junction_name = 'comp_firfilter';
        model.sim   = list(junction_name, 5);
        model.in    = 1;
        model.in2   = 1;
        model.intyp = 1;
        model.out   = 1;
        model.out2  = 1;
        model.outtyp= 1;
        model.label = ['firfilter'];
        model.state = zeros(size(model.rpar));

        model.blocktype='c';
        model.dep_ut=[%t %f]; //depends on input, not on time

        exprs = [sci2exp(1);..
                 sci2exp([0.5 0.5]);..
                 'bp';..   //option 2 start
                 sci2exp(55);..
                 sci2exp([0.15 0.35]);..
                 'ch';..
                 sci2exp([0.001 -1]);..
                 sci2exp([0,1,1,1,0,0,0]);..  //option 3 start
                 sci2exp(21);..             //option 4 start
                 '.5*(0:(21*16-1))/(21*16-1)';..
                 '[ones(1:21*16/3) 0*ones(1,2*21*16/3)]';..
                 sci2exp(32);..             //option 5 start
                 '[0 .2; .25 .35; .4 .5]';..
                 '[0 1 0]'..
                 ];
        gr_i = [''];

        x=standard_define([2 2],model,exprs,gr_i);
    end
endfunction
