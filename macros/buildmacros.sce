// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME, macros_path);
  tbx_build_blocks(toolbox_dir, ["xcos_mean" "xcos_variance" "xcos_std" "xcos_rms" "xcos_max" "xcos_min" "xcos_fft" "BUFFER_VECT" "xcos_plot" "xcos_runningmean" "xcos_firfilter" "xcos_iirfilter"]);
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

