function [x,y,typ] = xcos_iirfilter(job,arg1,arg2)

    x=[];y=[];typ=[];


    select job

    case 'plot' then
        standard_draw(arg1);

    case 'getinputs' then
        [x,y,typ]=standard_inputs(arg1);

    case 'getoutputs' then
        [x,y,typ]=standard_outputs(arg1);

    case 'getorigin' then
        [x,y]=standard_origin(arg1);

    case 'set' then
        x=arg1;
        model = arg1.model;
        graphics = arg1.graphics;
        exprs = graphics.exprs;

        ok = %t;
        while %t do

            [ok, option, exprs(1)] = getvalue(['How to design filter?'; '1. Enter coefficients manually'; '2. Use Scilab iir function'; '3. Use Scilab eqiir function'],..
            ['Enter option'],..
            list('vec',1), exprs(1));

            if (option<1 | option>3)
                message("Enter option 1 to 3");
                ok = %f;
            end

            if ~ok
                break;
            end

            if (option==1) then
                [ok, numcoeff, dencoeff, exprs(2:3)] = getvalue("Set filter coefficients starting from constant term",..
                             ["Numerator coefficients"; "Denominator coefficients"],..
                             list("vec", -1, "vec", -1), exprs(2:3));
            end

            if (option==2) then
                [ok, n, ftype, fdesign, frq, delta, exprs(4:8)] = getvalue('Enter filter parameters (Refer to help page of iir function)',..
                ['Order of filter (n)';..
                'Type of filter (lp:lowpass, hp:highpass, bp:bandpass, sb:bandstop';..
                'Design of filter (butt:Butterworth, cheb1:Chebyshev Type I, cheb2:Chebyshev Type II, ellip:Elliptic)';..
                'Normalized cutoff frequencies (between 0 and 0.5)';..
                'Ripple values (delta)';..
                ],..
                list('vec', 1, 'str',-1, 'str', -1, 'vec', -1, 'vec', -1),..
                exprs(4:8));

                hz = iir(n, ftype, fdesign, frq, delta);
                //
                s = poly(0,'z');
                hz = horner(hz, 1/s);
                //These two lines are needed because Scilab constructs the transfer function in powers of z instead of z^-1.
                numcoeff = coeff(hz.num);
                dencoeff = coeff(hz.den);
            end

            if (option==3) then
                [ok, ftype, approx, om, deltap, deltas, exprs(9:13)] = getvalue('Enter filter parameters (Refer to help page of eqiir function)',..
                ['Type of filter (lp:lowpass, hp:highpass, bp:bandpass, sb:bandstop';..
                'Design of filter (butt:Butterworth, cheb1:Chebyshev Type I, cheb2:Chebyshev Type II, ellip:Elliptic)';..
                'Four-vector of cutoff frequencies (between 0 and pi)';..
                'Passband ripple (deltap)';..
                'Stopband ripple (deltas)'
                ],..
                list('str', -1, 'str',-1, 'vec', -1, 'vec', 1, 'vec', 1),..
                exprs(9:13));

                [cells, fact, zzeros, zpoles] = eqiir(ftype, approx, om, deltap, deltas);
                hz = fact * poly(zzeros,'z') / poly(zpoles,'z');
                s = poly(0,'z');
                hz = horner(hz, 1/s);
                numcoeff = coeff(hz.num);
                dencoeff = coeff(hz.den);
            end

            if ~ok
                break;
            end

            numcoeff = numcoeff/dencoeff(1);
            dencoeff = -dencoeff/dencoeff(1);
            coeffstruct = [length(numcoeff) numcoeff dencoeff(2:$)];
            model.rpar = coeffstruct;
            model.state = zeros(1,length(coeffstruct)-1);
            junction_name = "comp_iirfilter";
            it = 1;
            ot = 1;
            in = [model.in model.in2];
            out = [model.in model.in2];

            if ok
                [model, graphics, ok] = set_io(model, graphics, list(in,it), list(out,ot), [], []);
            end

            if ok then
                model.sim       = list(junction_name, 5);
                arg1.model      = model;
                graphics.exprs  = exprs;
                arg1.graphics   = graphics;
                x               = arg1;
                break;
            end
        end

    case 'define' then
        model=scicos_model();
        junction_name = 'comp_iirfilter';
        model.sim   = list(junction_name, 5);
        model.in    = 1;
        model.in2   = 1;
        model.intyp = 1;
        model.out   = 1;
        model.out2  = 1;
        model.outtyp= 1;
        model.label = ['iirfilter'];
        model.state = zeros(size(model.rpar));

        model.blocktype='c';
        model.dep_ut=[%t %f]; //depends on input, not on time

        exprs = [sci2exp(1);..
                 sci2exp([0.25 0.25]);..
                 sci2exp([1 -0.5]);..
                 sci2exp(5);..
                 'lp';..
                 'cheb1';..
                 sci2exp([0.2 0]);..
                 sci2exp([0.05 0.05]);..
                 'lp';..
                 'ellip';..
                 '[2*%pi/10, 4*%pi/10]';..
                 sci2exp(0.02);..
                 sci2exp(0.001)..
                 ];

        gr_i = [''];

        x=standard_define([2 2],model,exprs,gr_i);
    end
endfunction
